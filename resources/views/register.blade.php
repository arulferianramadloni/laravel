@extends('layout.master')

@section('judul')
    Halaman Form
@endsection

@section('content')
    <h4>Buat Account Baru</h4>
    <h5>Sign Up Form</h5>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name :</label> <br>
        <input type="text" name="firstname"> <br>
        <label>Last Name :</label> <br>
        <input type="text" name="lastname"> <br>
        <label>Gender</label> <br>
        <input type="radio" name="jk"> Male <br>
        <input type="radio" name="jk"> Female <br>
        <label>Nationality</label> <br>
        <select name="" id="">
            <option value="1">Indonesia</option>
            <option value="2">England</option>
            <option value="3">Netherlands</option>
        </select> <br>
        <label>Language Spoken</label> <br>
        <input type="checkbox" value="11" name="language"> Bahasa Indonesia <br>
        <input type="checkbox" value="11" name="language"> English <br>
        <input type="checkbox" value="11" name="language"> Other <br>
        <label>Bio</label> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up" name="signup">
    </form>
@endsection
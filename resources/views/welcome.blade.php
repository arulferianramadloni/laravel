@extends('layout.master')

@section('judul')
    Halaman Index
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$firstname}} {{$lastname}} </h1>
    <p>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</p>
@endsection